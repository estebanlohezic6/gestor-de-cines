package com.mycompany.gestordecines;


import java.sql.Connection;
import java.sql.SQLException;
import modelos.PeliculasDTU;
import utils.Conexion;

/*
 * @author Alumno
 */
public class GestorDeCine {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Conexion conectar = new Conexion();
        Connection conectado = conectar.obtener();
        Pelicula servicio = new Pelicula();
        PeliculasDTU recuperarPorId = servicio.recuperarPorId(conectado, 5);
        servicio.recuperarTodas(conectado);
        Genero servicio2 = new Genero();
        servicio2.recuperarTodos(conectado);
        PaisDeOrigen servicio3 = new PaisDeOrigen();
        servicio3.recuperarTodos(conectado);
        Calificacion servicio4 = new Calificacion();
        servicio4.recuperarTodas(conectado);
    }
}
