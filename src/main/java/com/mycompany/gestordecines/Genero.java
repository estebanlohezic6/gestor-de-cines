package com.mycompany.gestordecines;

/*
 * @author LOHEZIC
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelos.GenerosDTU;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtil;

public class Genero {

    private Session sesion;
    private Transaction tx;

    private void iniciaOperacion() throws HibernateException {
        sesion = HibernateUtil.getSessionFactory().openSession();
        tx = sesion.beginTransaction();
    }

    private void manejaExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he);
    }

    public List<GenerosDTU> recuperarTodos(Connection conexion) throws SQLException {
        List<GenerosDTU> generos = new ArrayList<>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT * FROM generos ORDER BY idGenero ASC");
            ResultSet resultado = consulta.executeQuery();
            System.out.println("Los géneros son:");
            while (resultado.next()) {
                generos.add(new GenerosDTU(resultado.getInt("idGenero"), resultado.getString("genero")));
                System.out.println("ID: " + resultado.getInt("idGenero") + "   Género: " + resultado.getString("genero"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
        return generos;
    }

    public int guardarGenero(GenerosDTU genero) {
        int id = 0;
        try {
            iniciaOperacion();
            id = (Integer) sesion.save(genero);
            tx.commit();
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
        return id;
    }

    public void agregarGenero(Connection conexion, String nombre) throws SQLException {
        try {
            iniciaOperacion();
            PreparedStatement consulta = conexion.prepareStatement("Insert into generos (idGenero,genero) "
                    + " values (null,'" + nombre + "')");
            int resultado = consulta.executeUpdate();
            if (resultado != 0) {
                System.out.println("Se agregó un nuevo género.");
            } else {
                System.out.println("No se pudo agregar el género.");
            }
            tx.commit();
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    public void eliminarGenero(Connection conexion, int id, String nombre) throws SQLException {
        if (id != 0) {
            PreparedStatement consulta = conexion.prepareStatement("Delete from generos where idGenero= " + id + ";");
            int resultado = consulta.executeUpdate();
            if (resultado != 0) {
                System.out.println("Se borró el género.");
            } else {
                System.out.println("No se pudo borrar el género.");
            }
        } else {
            PreparedStatement consulta = conexion.prepareStatement("Delete from generos where nombre = '" + nombre + "';");
            int resultado = consulta.executeUpdate();
            if (resultado != 0) {
                System.out.println("Se borró el género.");
            } else {
                System.out.println("No se pudo borrar el género.");
            }
        }
    }

    public void modificarGenero(Connection conexion, int id, String nombre) throws SQLException {
        PreparedStatement consulta = conexion.prepareStatement("Update generos set genero = '" + nombre + "'"
                + " where idGenero = " + id);
        int resultado = consulta.executeUpdate();
        if (resultado != 0) {
            System.out.println("Se actualizó un género.");
        } else {
            System.out.println("No se pudo actualizar.");
        }
    }
}
