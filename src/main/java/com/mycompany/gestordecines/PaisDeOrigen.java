package com.mycompany.gestordecines;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelos.PaisDeOrigenDTU;

/**
 *
 * @author LOHEZIC
 */
public class PaisDeOrigen {

    public List<PaisDeOrigenDTU> recuperarTodos(Connection conexion) throws SQLException {
        List<PaisDeOrigenDTU> paises = new ArrayList<>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("Select * from paisdeorigen"
                    + " order by idPais ASC");
            ResultSet resultado = consulta.executeQuery();
            System.out.println("Los paises son:");
            while (resultado.next()) {
                paises.add(new PaisDeOrigenDTU(resultado.getInt("idPais"), resultado.getString("pais"),
                        resultado.getString("idioma")));
                System.out.println("ID: " + resultado.getInt("idPais")
                        + "   Idioma:" + resultado.getString("idioma")
                        + "   País: " + resultado.getString("pais"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
        return paises;
    }

    public void agregarPais(Connection conexion, String idioma, String pais) throws SQLException {
        PreparedStatement consulta = conexion.prepareStatement("Insert into paisdeorigen "
                + "(idPais,idioma,pais) values (null,'"
                + idioma + "','" + pais + "');");
        int resultado = consulta.executeUpdate();
        if (resultado != 0) {
            System.out.println("Se agregó un país.");
        } else {
            System.out.println("No se pudo agregar un país.");
        }
    }
    
    public void eliminarPais(Connection conexion, int id, String pais) throws SQLException{
        if (id != 0){
            PreparedStatement consulta = conexion.prepareStatement("Delete from paisdeorigen "
                    + "where idPais = " + id);
            int resultado = consulta.executeUpdate();
            if (resultado != 0) {
                System.out.println("Se borró un país.");
            } else {
                System.out.println("No se pudo borrar un país.");
            }
        } else {
            PreparedStatement consulta = conexion.prepareStatement("Delete from "
                + "paisdeorigen where pais = '" + "';");
            int resultado = consulta.executeUpdate();
            if (resultado != 0) {
                System.out.println("Se borró un país.");
            } else {
                System.out.println("No se pudo borrar un país.");
            }
        }
    }
    
    public void modificarPais (Connection conexion, int id, String idioma, String pais)
            throws SQLException{
        PreparedStatement consulta = conexion.prepareStatement("Update paisdeorigen set idioma = '"
                + idioma + "', pais = '"
                + pais + "' where idPais = " + id + ";");
        int resultado = consulta.executeUpdate();
        if (resultado != 0) {
            System.out.println("Se actualizó un país.");
        } else {
            System.out.println("No se pudo actualizar un país.");
        }
    }
}
