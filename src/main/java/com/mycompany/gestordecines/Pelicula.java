package com.mycompany.gestordecines;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import modelos.PeliculasDTU;
import modelos.GenerosDTU;
import modelos.CalificacionDTU;
import modelos.PaisDeOrigenDTU;

/*
 * @author Alumno
 */
public class Pelicula {

    public List<PeliculasDTU> recuperarTodas(Connection conexion) throws SQLException {
        List<PeliculasDTU> peliculas = new ArrayList<>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT * FROM pelicula join generos join calificacion join paisDeOrigen"
                    + " where pelicula.idCalificacionFK = calificacion.idCalificacion and"
                    + " pelicula.idGeneroFK = generos.idGenero and"
                    + " pelicula.idPaisFK = paisdeorigen.idPais ORDER BY idPelicula ASC");
            ResultSet resultado = consulta.executeQuery();
            System.out.println("Las películas son:");
            while (resultado.next()) {
                GenerosDTU genero = new GenerosDTU(resultado.getInt("idGenero"),
                        resultado.getString("genero"));
                PaisDeOrigenDTU pais = new PaisDeOrigenDTU(resultado.getInt("idPais"), 
                        resultado.getString("idioma"), resultado.getString("pais"));
                CalificacionDTU calif = new CalificacionDTU(resultado.getInt("idCalificacion"), 
                        resultado.getString("calificacion"));
                peliculas.add(new PeliculasDTU(resultado.getInt("idPelicula"),
                        resultado.getString("nombre"), resultado.getString("tituloOriginal"),
                        resultado.getString("duracion"), resultado.getString("añoEstreno"),
                        resultado.getBoolean("disponible"), resultado.getDate("fechaIngreso"),
                        genero, calif, pais));
                System.out.println("ID: " + resultado.getInt("idPelicula") + "   Titulo: " + resultado.getString("nombre"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
        return peliculas;
    }

    public PeliculasDTU recuperarPorId(Connection conexion, int id) throws SQLException {
        PeliculasDTU miPelicula = null;
        PreparedStatement consulta = conexion.prepareStatement("Select * from pelicula join calificacion join generos join paisdeorigen where idPelicula = " + id
                + " and pelicula.idCalificacionFK = calificacion.idCalificacion and"
                + " pelicula.idGeneroFK = generos.idGenero and"
                + " pelicula.idPaisFK = paisdeorigen.idPais order by idPelicula ASC");
        ResultSet resultado = consulta.executeQuery();
        while (resultado.next()) {
            GenerosDTU genero = new GenerosDTU(resultado.getInt("idGenero"),
                        resultado.getString("genero"));
                PaisDeOrigenDTU pais = new PaisDeOrigenDTU(resultado.getInt("idPais"), 
                        resultado.getString("idioma"), resultado.getString("pais"));
                CalificacionDTU calif = new CalificacionDTU(resultado.getInt("idCalificacion"), 
                        resultado.getString("calificacion"));
            miPelicula = new PeliculasDTU(id, resultado.getString("nombre"),
                    resultado.getString("tituloOriginal"), resultado.getString("duracion"),
                    resultado.getString("añoEstreno"), resultado.getBoolean("disponible"),
                    resultado.getDate("fechaIngreso"), genero, calif, pais);
            System.out.println("ID: " + id);
            System.out.println("Título: " + resultado.getString("nombre"));
            System.out.println("Título Original: " + resultado.getString("tituloOriginal"));
            System.out.println("Duracion: " + resultado.getString("duracion"));
            System.out.println("Año de Estreno: " + resultado.getString("añoEstreno"));
            System.out.println("Fecha Ingreso: " + resultado.getDate("fechaIngreso"));
            System.out.println("Genero: " + resultado.getString("genero"));
            System.out.println("Calificacion: " + resultado.getString("calificacion"));
            System.out.println("País de Origen: " + resultado.getString("pais"));
            System.out.println("Idioma: " + resultado.getString("idioma"));
            if (resultado.getBoolean("disponible") == true) {
                System.out.println("Estado: Disponible");
            } else {
                System.out.println("Estado: No Disponible");
            }
        }
        return miPelicula;
    } // 10:45

    public void agregarPelicula(Connection conexion, String nombre, String tituloOriginal, String duracion, String añoEstreno, Boolean disponible, Date fechaIngreso, int idGeneroFK, int idCalificacionFK, int idPaisFK) throws SQLException {
        PreparedStatement consulta = conexion.prepareStatement("Insert into pelicula (idPelicula,nombre,tituloOriginal,\n"
                + "duracion,añoEstreno,disponible,fechaIngreso,\n"
                + "idGeneroFK,idCalificacionFK,idPaisFK) values ("
                + null + ",'" + nombre + "','" + tituloOriginal
                + "','" + duracion + "','" + añoEstreno + "'," + disponible + ", "
                + fechaIngreso + "," + idGeneroFK + ","
                + idCalificacionFK + "," + idPaisFK + ");");
        int resultado = consulta.executeUpdate();
        if (resultado != 0) {
            System.out.println("Se agregó la película.");
        } else {
            System.out.println("No se pudo agregar la película.");
        }
    }

    public void eliminarPelicula(Connection conexion, int id, String nombre) throws SQLException {
        if (id != 0) {
            PreparedStatement consulta = conexion.prepareStatement("Delete from pelicula where idPelicula= " + id + ";");
            int resultado = consulta.executeUpdate();
            if (resultado != 0) {
                System.out.println("Se borró la película.");
            } else {
                System.out.println("No se pudo borrar la película.");
            }
        } else {
            PreparedStatement consulta = conexion.prepareStatement("Delete from pelicula where nombre = '" + nombre + "';");
            int resultado = consulta.executeUpdate();
            if (resultado != 0) {
                System.out.println("Se borró la película.");
            } else {
                System.out.println("No se pudo borrar la película.");
            }
        }
    }

    public void modificarPelicula(Connection conexion, int id, String nombre, String tituloOriginal, String duracion, String añoEstreno, Boolean disponible, Date fechaIngreso, int idGeneroFK, int idCalificacionFK, int idPaisFK) throws SQLException {
        PreparedStatement consulta = conexion.prepareStatement("Update pelicula set nombre = '"
                + nombre + "', tituloOriginal = '" + tituloOriginal
                + "', duracion = '" + duracion + "', añoEstreno = '"
                + añoEstreno + "', disponible = " + disponible + ", fechaIngreso = "
                + fechaIngreso + ", idGeneroFK = " + idGeneroFK + ", idCalificacionFK = "
                + idCalificacionFK + ", idPaisFK = " + idPaisFK + " where idPelicula = " + id);
        int resultado = consulta.executeUpdate();
        if (resultado != 0) {
            System.out.println("Se actualizó la película.");
        } else {
            System.out.println("No se pudo actualizar la película.");
        }
    }
}
