package com.mycompany.gestordecines;

/**
 *
 * @author LOHEZIC
 */
import modelos.CalificacionDTU;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Calificacion {

    public List<CalificacionDTU> recuperarTodas(Connection conexion)
            throws SQLException {
        List<CalificacionDTU> calificaciones = new ArrayList<>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT * FROM "
                    + "calificacion");
            ResultSet resultado = consulta.executeQuery();
            System.out.println("Las calificaciones de edad son: ");
            while (resultado.next()) {
                calificaciones.add(new CalificacionDTU(resultado.getInt("idCalificacion"),
                        resultado.getString("calificacion")));
                System.out.println("ID: " + resultado.getInt("idCalificacion")
                        + "   Calificacion: " + resultado.getString("calificacion"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
        return calificaciones;
    }

    public void agregarCalificacion(Connection conexion, String calificacion)
            throws SQLException {
        PreparedStatement consulta = conexion.prepareStatement("Insert into "
                + "calificacion (idCalificacion, calificacion) values"
                + " (null, '" + calificacion + "');");
        int resultado = consulta.executeUpdate();
        if (resultado != 0) {
            System.out.println("Se agregó la calificación.");
        } else {
            System.out.println("No se pudo agregar la calificación.");
        }
    }

    public void eliminarCalificacion(Connection conexion, int id, String calificacion)
            throws SQLException {
        if (id != 0) {
            PreparedStatement consulta = conexion.prepareStatement("Delete from calificacion "
                    + " where idCalificacion = " + id);
            int resultado = consulta.executeUpdate();
            if (resultado != 0) {
                System.out.println("Se borró la calificación.");
            } else {
                System.out.println("No se pudo borrar la calificación.");
            }
        } else {
            PreparedStatement consulta = conexion.prepareStatement("Delete from "
                    + " calificacion where calificacion = '" + calificacion + "';");
            int resultado = consulta.executeUpdate();
            if (resultado != 0) {
                System.out.println("Se borró la calificación.");
            } else {
                System.out.println("No se pudo borrar la calificación.");
            }
        }
    }

    public void modificarCalificacion(Connection conexion, int id, String calificacion)
            throws SQLException {
        PreparedStatement consulta = conexion.prepareStatement("Update calificacion "
                + "set calificacion = '" + calificacion + "' where "
                + "idCalificacion = " + id);
        int resultado = consulta.executeUpdate();
        if (resultado != 0) {
            System.out.println("Se actualizó la calificación.");
        } else {
            System.out.println("No se pudo actualizar la calificación.");
        }
    }
}
