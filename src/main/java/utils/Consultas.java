package utils;
import modelos.GenerosDTU;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Consultas {

    private final String tabla = "generos";

    public GenerosDTU recuperarPorId(Connection conexion, int id) throws SQLException {
        GenerosDTU genero = null;
            PreparedStatement consulta = conexion.prepareStatement("SELECT nombregen FROM" + this.tabla + "WHERE id = ?");
            ResultSet resultado = consulta.executeQuery();
            while(resultado.next()){
                genero = new GenerosDTU(id, resultado.getString("nombregen"));
            }
        return genero;
    }
    
    public List<GenerosDTU> recuperarTodas(Connection conexion) throws SQLException{
        List<GenerosDTU> generos = new ArrayList<>();
        try{
            PreparedStatement consulta = conexion.prepareStatement("SELECT id_genero, nombre FROM" + this.tabla + " ORDER BY id_genero ASC");
            ResultSet resultado = consulta.executeQuery();
            while(resultado.next()){
                generos.add(new GenerosDTU(resultado.getInt("id_genero"), resultado.getString("nombre")));
            }
        } catch(SQLException ex){
            throw new SQLException(ex);
        }
        return generos;
    }
}
