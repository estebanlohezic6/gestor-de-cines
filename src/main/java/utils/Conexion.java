package utils;

import java.sql.*;

public class Conexion {

    private Connection cnx = null;
    public Statement stm;
    public ResultSet rs;
    private String user = "root";
    private String password = "43562884356288";
    private String url = "jdbc:mysql://localhost:3306/gestorcines";

    public Connection obtener() throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = null;
            connection = DriverManager.getConnection(url, user, password);
            boolean valid = connection.isValid(50000);
            System.out.println(valid ? "TEST OK - Conectado " : "TEST FAIL - No conectado");
            return connection;

        } catch (ClassNotFoundException ex) {
            System.out.println("Error al registrar el driver de MySQL: " + ex);
            return null;
        } catch (java.sql.SQLException sqle) {
            System.out.println("Error: " + sqle);
            return null;
        }
    }

    public void cerrar() throws SQLException {
        if (cnx != null) {
            cnx.close();
        }
    }
}
