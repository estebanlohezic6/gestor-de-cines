package modelos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * @author LOHEZIC
 */
@Entity
@Table(name = "calificacion")
public class CalificacionDTU implements Serializable {

    @Id
    @Column(name = "idCalificacion")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idCalificacion;

    @Column(name = "calificacion")
    private String calificacion;

    public CalificacionDTU() {
    }

    public CalificacionDTU(int idCalificacion, String calificacion) {
        this.idCalificacion = idCalificacion;
        this.calificacion = calificacion;
    }

    /**
     * @return the calificacion
     */
    public String getCalificacion() {
        return calificacion;
    }

    /**
     * @param calificacion the calificacion to set
     */
    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    /**
     * @return the idCalificacion
     */
    public int getIdCalificacion() {
        return idCalificacion;
    }

    /**
     * @param idCalificacion the idCalificacion to set
     */
    public void setIdCalificacion(int idCalificacion) {
        this.idCalificacion = idCalificacion;
    }
}
