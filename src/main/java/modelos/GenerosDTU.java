package modelos;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "generos")
public class GenerosDTU implements Serializable {

    @Id
    @Column(name = "idGenero")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idGenero;

    @Column(name = "Genero")
    private String nombre;

    public GenerosDTU() {
        this.idGenero = null;
        this.nombre = null;
    }

    public GenerosDTU(Integer idGenero, String nombre) {
        this.idGenero = idGenero;
        this.nombre = nombre;
    }

    /**
     * @return the idGenero
     */
    public Integer getId_genero() {
        return idGenero;
    }

    /**
     * @param idGenero the idGenero to set
     */
    public void setId_genero(Integer idGenero) {
        this.idGenero = idGenero;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
