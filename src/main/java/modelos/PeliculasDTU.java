
package modelos;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author LOHEZIC
 */

@Entity
@Table(name = "pelicula")
public class PeliculasDTU implements Serializable{
   
    @Id
    @Column(name = "idPelicula")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idPelicula;
    
    @Column(name = "nombre")
    private String nombre;
    
    @Column(name = "tituloOriginal") 
    private String tituloOriginal;
    
    @Column(name = "duracion")
    private String duracion;
    
    @Column(name = "añoEstreno")
    private String añoEstreno;
   
    @Column(name = "disponible")
    private Boolean disponible;
    
    @Column(name = "fechaIngreso")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaIngreso;
    
    @OneToOne
    @JoinColumn(name = "idGenero")
    private GenerosDTU genero;
    
    @OneToOne
    @JoinColumn(name = "idCalificacion")
    private CalificacionDTU calificacion;
    
    @OneToOne
    @JoinColumn(name = "idPais")
    private PaisDeOrigenDTU pais;

    public PeliculasDTU(int idPelicula, String nombre, String tituloOriginal, String duracion, String añoEstreno, Boolean disponible, Date fechaIngreso, GenerosDTU genero, CalificacionDTU calificacion, PaisDeOrigenDTU pais) {
        this.idPelicula = idPelicula;
        this.nombre = nombre;
        this.tituloOriginal = tituloOriginal;
        this.duracion = duracion;
        this.añoEstreno = añoEstreno;
        this.disponible = disponible;
        this.fechaIngreso = fechaIngreso;
        this.genero = genero;
        this.calificacion = calificacion;
        this.pais = pais;
    }

    public PeliculasDTU() {
    }

    /**
     * @return the añoEstreno
     */
    public String getAñoEstreno() {
        return añoEstreno;
    }

    /**
     * @param añoEstreno the añoEstreno to set
     */
    public void setAñoEstreno(String añoEstreno) {
        this.añoEstreno = añoEstreno;
    }

    /**
     * @return the disponible
     */
    public Boolean getDisponible() {
        return disponible;
    }

    /**
     * @param disponible the disponible to set
     */
    public void setDisponible(Boolean disponible) {
        this.disponible = disponible;
    }

    /**
     * @return the duracion
     */
    public String getDuracion() {
        return duracion;
    }

    /**
     * @param duracion the duracion to set
     */
    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    /**
     * @return the fechaIngreso
     */
    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    /**
     * @param fechaIngreso the fechaIngreso to set
     */
    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the tituloOriginal
     */
    public String getTituloOriginal() {
        return tituloOriginal;
    }

    /**
     * @param tituloOriginal the tituloOriginal to set
     */
    public void setTituloOriginal(String tituloOriginal) {
        this.tituloOriginal = tituloOriginal;
    }

    /**
     * @return the idPelicula
     */
    public int getIdPelicula() {
        return idPelicula;
    }

    /**
     * @param idPelicula the idPelicula to set
     */
    public void setIdPelicula(int idPelicula) {
        this.idPelicula = idPelicula;
    }

    /**
     * @return the genero
     */
    public GenerosDTU getGenero() {
        return genero;
    }

    /**
     * @param genero the genero to set
     */
    public void setGenero(GenerosDTU genero) {
        this.genero = genero;
    }

    /**
     * @return the calificacion
     */
    public CalificacionDTU getCalificacion() {
        return calificacion;
    }

    /**
     * @param calificacion the calificacion to set
     */
    public void setCalificacion(CalificacionDTU calificacion) {
        this.calificacion = calificacion;
    }

    /**
     * @return the pais
     */
    public PaisDeOrigenDTU getPais() {
        return pais;
    }

    /**
     * @param pais the pais to set
     */
    public void setPais(PaisDeOrigenDTU pais) {
        this.pais = pais;
    }

    
}
