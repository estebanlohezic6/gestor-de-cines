package modelos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * @author LOHEZIC
 */
@Entity
@Table(name = "paisdeorigen")
public class PaisDeOrigenDTU implements Serializable {

    @Id
    @Column(name = "idPais")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idPais;

    @Column(name = "idioma")
    private String idioma;

    @Column(name = "pais")
    private String nombre;

    public PaisDeOrigenDTU() {
    }

    public PaisDeOrigenDTU(int idPais, String idioma, String nombre) {
        this.idPais = idPais;
        this.idioma = idioma;
        this.nombre = nombre;
    }

    /**
     * @return the idioma
     */
    public String getIdioma() {
        return idioma;
    }

    /**
     * @param idioma the idioma to set
     */
    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the idPais
     */
    public int getIdPais() {
        return idPais;
    }

    /**
     * @param idPais the idPais to set
     */
    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }
}
